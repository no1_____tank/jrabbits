package com.jrabbits.api.sys.service;

import java.util.List;

import com.jrabbits.api.sys.entity.Office;
import com.jrabbits.common.persistence.Page;

/**
 * 机构Service
 * @author jrabbits
 * @version 2014-05-16
 */
public interface OfficeService{
	/**
	 * 获取单条数据
	 * @param id
	 * @return
	 */
	public Office get(String id);
	
	/**
	 * 获取单条数据
	 * @param entity
	 * @return
	 */
	public Office get(Office office);
	
	/**
	 * 查询列表数据
	 * @param entity
	 * @return
	 */
	public List<Office> findList(Office office);
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param entity
	 * @return
	 */
	public Page<Office> findPage(Page<Office> page, Office office);

	/**
	 * 保存数据（插入或更新）
	 * @param entity
	 */
	public void save(Office office);
	
	/**
	 * 删除数据
	 * @param entity
	 */
	public void delete( Office office);
	//public List<Office> findAll();
	public List<Office> findAllList(Office office);
	//public List<Office> findList(Boolean isAll);
}
