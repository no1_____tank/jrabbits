package com.jrabbits.api.sys.service;

import com.jrabbits.api.sys.entity.User;

public interface CurrentUserSevice {

	public User getCurrentUser();
}
