package com.jrabbits.modules.sys.Cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Role;
import com.jrabbits.api.sys.entity.User;
import com.jrabbits.api.sys.service.SystemService;
import com.jrabbits.api.sys.service.UserService;
import com.jrabbits.api.sys.utils.UserUtils;
import com.jrabbits.common.constant.CacheDict;
import com.jrabbits.common.utils.CacheUtils;
import com.jrabbits.common.utils.GeneralPassWord;
import com.jrabbits.common.utils.StringUtils;
@Service("SystemCache")
@Transactional(readOnly = true)
public class SystemCache {
	
	@Autowired
	private SystemService systemService;
	@Autowired
	private UserService userService;
	/**
	 * 获取用户
	 * @param id
	 * @return
	 */
	public User getUser(String id) {
		return UserUtils.get(id);
	}
	/**
	 * 通过部门ID获取用户列表，仅返回用户id和name（树查询用户时用）
	 * @param user
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<User> findUserByOfficeId(String officeId) {
		List<User> list = (List<User>)CacheUtils.get(CacheDict.USER_CACHE, CacheDict.USER_CACHE_LIST_BY_OFFICE_ID_ + officeId);
		if (list == null){
			list = systemService.findUserByOfficeId(officeId);
			CacheUtils.put(CacheDict.USER_CACHE, CacheDict.USER_CACHE_LIST_BY_OFFICE_ID_ + officeId, list);
		}
		return list;
	}
	
	@Transactional(readOnly = false)
	public void saveUser(User user) {
		
		if (StringUtils.isNoneBlank(user.getId())){
			// 清除原用户机构用户缓存
			User oldUser = userService.get(user.getId());
			if (oldUser.getOffice() != null && oldUser.getOffice().getId() != null){
				CacheUtils.remove(CacheDict.USER_CACHE, CacheDict.USER_CACHE_LIST_BY_OFFICE_ID_ + oldUser.getOffice().getId());
			}
		}
		
		systemService.saveUser(user);
		
		if (StringUtils.isNotBlank(user.getId())){
			// 更新用户与角色关联
			// 将当前用户同步到Activiti
			//saveActivitiUser(user);
			// 清除用户缓存
			UserUtils.clearCache(user);
//			// 清除权限缓存
//			systemRealm.clearAllCachedAuthorizationInfo();
		}
	}
	
	@Transactional(readOnly = false)
	public void updateUserInfo(User user) {
		//user.preUpdate();
		systemService.updateUserInfo(user);
		// 清除用户缓存
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public void deleteUser(User user) {
		userService.delete(user);
		// 同步到Activiti
	//	deleteActivitiUser(user);
		// 清除用户缓存
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	
	@Transactional(readOnly = false)
	public void updatePasswordById(String id, String loginName, String newPassword) {
		systemService.updatePasswordById(id, loginName, newPassword);
		User user = new User(id);
		user.setPassword(GeneralPassWord.entryptPassword(newPassword));
		user.setLoginName(loginName);
		UserUtils.clearCache(user);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
	/**
	 * 根据登录名获取用户
	 * @param loginName
	 * @return
	 */
	public User getUserByLoginName(String loginName){
		return UserUtils.getByLoginName(loginName);
	}
	public List<Role> findAllRole(){
		return UserUtils.getRoleList();
	}
	
	@Transactional(readOnly = false)
	public void saveRole(Role role) {
		systemService.saveRole(role);
		// 同步到Activiti
		//saveActivitiGroup(role);
		// 清除用户角色缓存
		UserUtils.removeCache(CacheDict.CACHE_ROLE_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}

	@Transactional(readOnly = false)
	public void deleteRole(Role role) {
		systemService.deleteRole(role);
		// 同步到Activiti
		//deleteActivitiGroup(role);
		// 清除用户角色缓存
		UserUtils.removeCache(CacheDict.CACHE_ROLE_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
	}
}
