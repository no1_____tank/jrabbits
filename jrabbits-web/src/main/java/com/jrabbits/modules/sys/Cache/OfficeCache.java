package com.jrabbits.modules.sys.Cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Office;
import com.jrabbits.api.sys.service.OfficeService;
import com.jrabbits.api.sys.utils.UserUtils;
import com.jrabbits.common.constant.CacheDict;
@Service("OfficeCache")
@Transactional(readOnly = true)
public class OfficeCache {

	@Autowired
	private OfficeService officeService;
	
	public void save(Office office) {
		officeService.save(office);
		UserUtils.removeCache(CacheDict.CACHE_OFFICE_LIST);
	}

	public void delete(Office office) {
		officeService.delete(office);
		UserUtils.removeCache(CacheDict.CACHE_OFFICE_LIST);
	}

	public List<Office> findAll() {
		return UserUtils.getOfficeList();
	}

	public List<Office> findList(Boolean isAll) {
		if (isAll != null && isAll){
			return UserUtils.getOfficeAllList();
		}else{
			return UserUtils.getOfficeList();
		}
	}

}
