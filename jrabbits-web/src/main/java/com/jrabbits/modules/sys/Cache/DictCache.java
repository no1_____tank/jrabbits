package com.jrabbits.modules.sys.Cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Dict;
import com.jrabbits.api.sys.service.DictService;
import com.jrabbits.common.constant.CacheDict;
import com.jrabbits.common.utils.CacheUtils;
@Service("DictCache")
@Transactional(readOnly = true)
public class DictCache {
	@Autowired
	private DictService dictService;
	@Transactional(readOnly = false)
	public void save(Dict dict) {
		dictService.save(dict);
		CacheUtils.remove(CacheDict.CACHE_DICT_MAP);
	}
	@Transactional(readOnly = false)
	public void delete(Dict dict) {
		dictService.delete(dict);
		CacheUtils.remove(CacheDict.CACHE_DICT_MAP);
	}
}
