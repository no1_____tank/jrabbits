package com.jrabbits.modules.sys.Cache;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Menu;
import com.jrabbits.api.sys.service.MenuService;
import com.jrabbits.api.sys.utils.UserUtils;
import com.jrabbits.common.constant.CacheDict;
import com.jrabbits.common.utils.CacheUtils;
@Service("MenuCache")
@Transactional(readOnly = true)
public class MenuCache {
	@Autowired
	private MenuService menuService;
	public List<Menu> findAllMenu(){
		return UserUtils.getMenuList();
	}
	@Transactional(readOnly = false)
	public void saveMenu(Menu menu) {
		menuService.saveMenu(menu);
		// 清除用户菜单缓存
		UserUtils.removeCache(CacheDict.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(CacheDict.CACHE_MENU_NAME_PATH_MAP);
	}

	@Transactional(readOnly = false)
	public void updateMenuSort(Menu menu) {
		menuService.updateMenuSort(menu);
		// 清除用户菜单缓存
		UserUtils.removeCache(CacheDict.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(CacheDict.CACHE_MENU_NAME_PATH_MAP);
	}
	@Transactional(readOnly = false)
	public void deleteMenu(Menu menu) {
		menuService.deleteMenu(menu);
		// 清除用户菜单缓存
		UserUtils.removeCache(CacheDict.CACHE_MENU_LIST);
//		// 清除权限缓存
//		systemRealm.clearAllCachedAuthorizationInfo();
		// 清除日志相关缓存
		CacheUtils.remove(CacheDict.CACHE_MENU_NAME_PATH_MAP);
	}
}
