/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/jrabbits/jrabbits">jrabbits</a> All rights reserved.
 */
package com.jrabbits.modules.sys.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jrabbits.api.sys.entity.Log;
import com.jrabbits.api.sys.service.LogService;
import com.jrabbits.common.persistence.Page;
import com.jrabbits.common.web.BaseController;

/**
 * 日志Controller
 * @author jrabbits
 * @version 2013-6-2
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/log")
public class LogController extends BaseController {

	@Autowired
	private LogService logService;
	
	@RequiresPermissions("sys:log:view")
	@RequestMapping(value = {"list", ""})
	public String list(Log log, HttpServletRequest request, HttpServletResponse response, Model model) {
        Page<Log> page = logService.findPage(new Page<Log>(request, response), log); 
        model.addAttribute("page", page);
		return "modules/sys/logList";
	}

}
