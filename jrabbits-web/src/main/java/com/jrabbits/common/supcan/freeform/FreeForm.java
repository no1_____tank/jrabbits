/**
 * Copyright &copy; 2012-2016 <a href="https://github.com/jrabbits/jrabbits">jrabbits</a> All rights reserved.
 */
package com.jrabbits.common.supcan.freeform;

import com.jrabbits.common.supcan.common.Common;
import com.jrabbits.common.supcan.common.properties.Properties;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 硕正FreeForm
 * @author WangZhen
 * @version 2013-11-04
 */
@XStreamAlias("FreeForm")
public class FreeForm extends Common {

	public FreeForm() {
		super();
	}
	
	public FreeForm(Properties properties) {
		this();
		this.properties = properties;
	}
	
}
