package com.jrabbits.impl.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Area;
import com.jrabbits.api.sys.service.AreaService;
import com.jrabbits.common.service.ServiceException;
import com.jrabbits.common.utils.Reflections;
import com.jrabbits.common.utils.StringUtils;
import com.jrabbits.impl.sys.dao.AreaDao;

/**
 * 区域Service
 * @author jrabbits
 * @version 2014-05-16
 */
@Service("AreaService")
@Transactional(readOnly = true)
public class AreaServiceImpl implements AreaService {
	@Autowired
	private AreaDao areaDao;
	public List<Area> findAll(){
		return areaDao.findAllList(new Area());
	}

	@Transactional(readOnly = false)
	public void save(Area area) {
	
	//	Class<Area> entityClass = Reflections.getClassGenricType(getClass(), 1);
		// 如果没有设置父节点，则代表为跟节点，有则获取父节点实体
		if (area.getParent() == null || StringUtils.isBlank(area.getParentId()) 
				|| "0".equals(area.getParentId())){
			area.setParent(null);
		}else{
			area.setParent(areaDao.get(area.getParentId()));
		}
		if (area.getParent() == null){
	//		Area parentEntity = null;
//			try {
//				parentEntity = entityClass.getConstructor(String.class).newInstance("0");
//			} catch (Exception e) {
//				throw new ServiceException(e);
//			}
			area.setParent(new Area());
			area.getParent().setParentIds(StringUtils.EMPTY);
		}
		
		// 获取修改前的parentIds，用于更新子节点的parentIds
		String oldParentIds = area.getParentIds(); 
		
		// 设置新的父节点串
		area.setParentIds(area.getParent().getParentIds()+area.getParent().getId()+",");
		
		// 保存或更新实体
		saveArea(area);
		
		// 更新子节点 parentIds
		Area o = null;
		try {
			o = new Area();
		} catch (Exception e) {
			throw new ServiceException(e);
		}
		o.setParentIds("%,"+area.getId()+",%");
		List<Area> list = areaDao.findByParentIdsLike(o);
		for (Area e : list){
			if (e.getParentIds() != null && oldParentIds != null){
				e.setParentIds(e.getParentIds().replace(oldParentIds, area.getParentIds()));
				preUpdateChild(area, e);
				areaDao.updateParentIds(e);
			}
		}
	}
	
	@Transactional(readOnly = false)
	public void delete(Area area) {
		areaDao.delete(area);
	}
	/**
	 * 保存数据（插入或更新）
	 * @param area
	 */
	@Transactional(readOnly = false)
	public void saveArea(Area area){
		if (area.getIsNewRecord()){
			areaDao.insert(area);
		}else{
			areaDao.update(area);
		}
	}
	/**
	 * 预留接口，用户更新子节前调用
	 * @param childEntity
	 */
	protected void preUpdateChild(Area entity, Area childEntity) {
		
	}

	@Override
	public Area get(String id) {
		return areaDao.get(id);
	}

	@Override
	public List<Area> findAllList(Area area) {
		
		return areaDao.findAllList(area);
	}
}
