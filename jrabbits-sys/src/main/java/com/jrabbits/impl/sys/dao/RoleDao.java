package com.jrabbits.impl.sys.dao;

import com.jrabbits.api.sys.entity.Role;
import com.jrabbits.common.persistence.CrudDao;
import com.jrabbits.common.persistence.annotation.MyBatisDao;

/**
 * 角色DAO接口
 * @author jrabbits
 * @version 2013-12-05
 */
@MyBatisDao
public interface RoleDao extends CrudDao<Role> {

	public Role getByName(Role role);
	
	public Role getByEnname(Role role);

	/**
	 * 维护角色与菜单权限关系
	 * @param role
	 * @return
	 */
	public int deleteRoleMenu(Role role);

	public int insertRoleMenu(Role role);
	
	/**
	 * 维护角色与公司部门关系
	 * @param role
	 * @return
	 */
	public int deleteRoleOffice(Role role);

	public int insertRoleOffice(Role role);

}
