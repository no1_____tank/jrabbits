package com.jrabbits.impl.sys.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jrabbits.api.sys.entity.Role;
import com.jrabbits.api.sys.service.RoleService;
import com.jrabbits.impl.sys.dao.RoleDao;

@Transactional(readOnly = true)
@Service("RoleService")
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleDao roleDao;
	
	public List<Role> findList(Role role) {
		return roleDao.findList(role);
	}
	
	public List<Role> findAllList(Role role) {
		return roleDao.findAllList(role);
	}

}
