package com.jrabbits.impl.sys.dao;

import java.util.List;

import com.jrabbits.api.sys.entity.Menu;
import com.jrabbits.common.persistence.CrudDao;
import com.jrabbits.common.persistence.annotation.MyBatisDao;

/**
 * 菜单DAO接口
 * @author jrabbits
 * @version 2014-05-16
 */
@MyBatisDao
public interface MenuDao extends CrudDao<Menu> {

	public List<Menu> findByParentIdsLike(Menu menu);

	public List<Menu> findByUserId(Menu menu);
	
	public int updateParentIds(Menu menu);
	
	public int updateSort(Menu menu);
	
}
